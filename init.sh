#!/bin/bash



rm -rf disque ramDisk usb1 usb2
mkdir disque ramDisk usb1 usb2
touch disque/file.txt



dd if=/dev/random bs=64 count=1 > ramDisk/key1.txt

dd if=/dev/random bs=64 count=1 > ramDisk/key2.txt



echo "----------key of responsable 1----------"
openssl enc -pbkdf2 -aes-256-cbc -nopad -in ramDisk/key1.txt -out usb1/key1.enc

echo "----------key of responsable 2----------"
openssl enc -pbkdf2 -aes-256-cbc -nopad -in ramDisk/key2.txt -out usb2/key2.enc



openssl enc -pbkdf2 -aes-256-cbc -nopad -in disque/file.txt -out disque/file1Crypt.enc -kfile ramDisk/key1.txt

openssl enc -pbkdf2 -aes-256-cbc -nopad -in disque/file1Crypt.enc  -out disque/fileCrypt.enc -kfile ramDisk/key2.txt


rm ramDisk/key1.txt
rm ramDisk/key2.txt
rm disque/file.txt
rm disque/file1Crypt.enc



