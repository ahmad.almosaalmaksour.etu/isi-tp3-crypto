#!/bin/bash


FILE=disque/file.txt 

echo "------ Révocation de droit ----------"
read -p "Qui va etre répuidié RES1,RES2,REP1,REP2 ? : " Answer


if [ $Answer = "RES1" ]
then
	if openssl enc -d -pbkdf2 -aes-256-cbc -nopad  -in usb1Rep/key1.enc -out ramDisk/key1.txt && openssl enc -d -pbkdf2 -aes-256-cbc -nopad -in usb2Rep/key2.enc -out ramDisk/key2.txt  && openssl enc -d -pbkdf2 -aes-256-cbc -nopad -in usb2/key2.enc -out ramDisk/key2.txt  
		
		then 

		
		sed "/I am represente/d" ramDisk/key1.txt > ramDisk/temp1.txt
		rm ramDisk/key1.txt
		mv ramDisk/temp1.txt ramDisk/key1.txt

		#décrypter le fichier paire
		openssl enc -d -pbkdf2 -aes-256-cbc -nopad -in disque/fileCrypt.enc -out disque/file1Crypt.enc -kfile ramDisk/key2.txt
		openssl enc -d -pbkdf2 -aes-256-cbc -nopad -in disque/file1Crypt.enc  -out disque/file.txt -kfile ramDisk/key1.txt
		rm disque/fileCrypt.enc
		rm disque/file1Crypt.enc


		# géneration de la nouvelle clé responsable1 et du representant

		dd if=/dev/random bs=64 count=1 > ramDisk/key1.txt
		cp ramDisk/key1.txt  ramDisk/key1Repr.txt
		sed -i 'I am represente' ramDisk/key1Repr.txt

		echo " key for the new resposable1"

		openssl enc -pbkdf2 -aes-256-cbc -nopad -in ramDisk/key1.txt -out usb1/key1.enc


		echo " key for the new represente1"
		openssl enc -pbkdf2 -aes-256-cbc -nopad -in ramDisk/key1Repr.txt -out usb1Rep/key1.enc


		#Recrypter le fichier paire avec les nouvelle clés
		openssl enc -pbkdf2 -aes-256-cbc -nopad -in disque/file.txt -out disque/file1Crypt.enc -kfile ramDisk/key1.txt
		openssl enc -pbkdf2 -aes-256-cbc -nopad -in disque/file1Crypt.enc -out disque/fileCrypt.enc -kfile ramDisk/key2.txt


		rm ramDisk/key1.txt
		rm ramDisk/key2.txt
		rm ramDisk/key1Repr.txt
		rm disque/file.txt
		rm disque/file1Crypt.enc


	else 
		echo "key not correct "
		
	fi

elif [ $Answer = "RES2" ]
then
	if openssl enc -d -pbkdf2 -aes-256-cbc -nopad  -in usb1Rep/key1.enc -out ramDisk/key1.txt && openssl enc -d -pbkdf2 -aes-256-cbc -nopad -in usb2Rep/key2.enc -out ramDisk/key2.txt  && openssl enc -d -pbkdf2 -aes-256-cbc -nopad -in usb1/key1.enc -out ramDisk/key1.txt  
		
		then 

		
		sed "/I am represente/d" ramDisk/key2.txt > ramDisk/temp2.txt
		rm ramDisk/key2.txt
		mv ramDisk/temp2.txt ramDisk/key2.txt

		openssl enc -d -pbkdf2 -aes256 -in disque/fileCrypt.enc -out disque/file1Crypt.enc -kfile ramDisk/key2.txt
		openssl enc -d -pbkdf2 -aes256 -in disque/file1Crypt.enc  -out disque/file.txt -kfile ramDisk/key1.txt
		rm disque/fileCrypt.enc
		rm disque/file1Crypt.enc



		dd if=/dev/random bs=64 count=1 > ramDisk/key2.txt
		cp ramDisk/key2.txt  ramDisk/key2Repr.txt
		sed -i 'I am represente' ramDisk/key2Repr.txt

		echo "key for the new resposable2"

		openssl enc -pbkdf2 -aes256 -in ramDisk/key2.txt -out usb1/key2.enc


		echo "key for the new represente1"
		openssl enc -pbkdf2 -aes256 -in ramDisk/key2Repr.txt -out usb1Rep/key2.enc


		openssl enc -pbkdf2 -aes256 -in disque/file.txt -out disque/file1Crypt.enc -kfile ramDisk/key1.txt
		openssl enc -pbkdf2 -aes256 -in disque/file1Crypt.enc -out disque/fileCrypt.enc -kfile ramDisk/key2.txt


		rm ramDisk/key1.txt
		rm ramDisk/key2.txt
		rm ramDisk/key2Repr.txt
		rm disque/file.txt
		rm disque/file1Crypt.enc


	else 
		echo "key not correct "
		
	fi
	
else
	echo "Responsable/Represente does not existe"

fi
